package com.practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaRepositories
public class OnlinePaymentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlinePaymentsApplication.class, args);
	}

}
